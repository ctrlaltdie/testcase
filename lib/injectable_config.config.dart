// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:dio/dio.dart' as _i3;
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;
import 'package:shared_preferences/shared_preferences.dart' as _i6;

import 'bloc/auth_bloc/auth_bloc_cubit.dart' as _i11;
import 'bloc/home_bloc/home_bloc_cubit.dart' as _i8;
import 'domain/i_auth.dart' as _i9;
import 'domain/i_home.dart' as _i4;
import 'infrastructure/database_module.dart' as _i14;
import 'infrastructure/dio_module.dart' as _i12;
import 'infrastructure/preferences_module.dart' as _i13;
import 'services/api_service.dart' as _i5;
import 'services/auth_services.dart' as _i10;
import 'services/user_database.dart'
    as _i7; // ignore_for_file: unnecessary_lambdas

// ignore_for_file: lines_longer_than_80_chars
/// initializes the registration of provided dependencies inside of [GetIt]
Future<_i1.GetIt> $initGetIt(_i1.GetIt get,
    {String? environment, _i2.EnvironmentFilter? environmentFilter}) async {
  final gh = _i2.GetItHelper(get, environment, environmentFilter);
  final dioModule = _$DioModule();
  final preferencesModule = _$PreferencesModule();
  final databaseModule = _$DatabaseModule();
  gh.factory<_i3.Dio>(() => dioModule.getDio());
  gh.factory<_i4.IHome>(() => _i5.ApiServices(get<_i3.Dio>()));
  await gh.factoryAsync<_i6.SharedPreferences>(() => preferencesModule.prefs,
      preResolve: true);
  gh.factory<_i7.UserDatabase>(() => databaseModule.database);
  gh.factory<_i8.HomeBlocCubit>(() => _i8.HomeBlocCubit(get<_i4.IHome>()));
  gh.factory<_i9.IAuth>(() =>
      _i10.AuthServices(get<_i6.SharedPreferences>(), get<_i7.UserDatabase>()));
  gh.factory<_i11.AuthBlocCubit>(() => _i11.AuthBlocCubit(get<_i9.IAuth>()));
  return get;
}

class _$DioModule extends _i12.DioModule {}

class _$PreferencesModule extends _i13.PreferencesModule {}

class _$DatabaseModule extends _i14.DatabaseModule {}
