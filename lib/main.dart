import 'package:get/get.dart';
import 'package:majootestcase/injectable_config.dart';
import 'package:majootestcase/routes.dart';
import 'package:majootestcase/ui/home_bloc/home_bloc_screen.dart';
import 'package:majootestcase/ui/login/login_page.dart';
import 'package:flutter/foundation.dart';
import 'package:majootestcase/ui/splash/splash_screen_page.dart';
import 'package:sqflite/sqflite.dart';
import 'bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/material.dart';

import 'services/user_database.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await configureDependencies();
  runApp(BlocProvider(
    create: (context) => getIt<AuthBlocCubit>(),
    child: MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Flutter Demo',
      getPages: Routes.getPages,
      theme: ThemeData(
        elevatedButtonTheme: ElevatedButtonThemeData(
          style: ButtonStyle(
            textStyle: MaterialStateProperty.all(TextStyle(fontSize: 18)),
            minimumSize: MaterialStateProperty.all(Size(double.infinity, 50)),
            foregroundColor: MaterialStateProperty.all(Colors.white),
            backgroundColor: MaterialStateProperty.resolveWith(
              (states) {
                if (states.contains(MaterialState.disabled)) {
                  return Colors.grey;
                }
                return Colors.black87;
              },
            ),
          ),
        ),
        visualDensity: VisualDensity.adaptivePlatformDensity,
        primarySwatch: Colors.red,
      ),

      initialRoute: SplashScreenPage.routeName,
      // home: BlocConsumer<AuthBlocCubit, AuthBlocState>(
      //     listener: (context, state) {
      //   print(state);
      //   print("TEST");
      //   if (state is AuthBlocLoginState) {
      //     Navigator.of(context).pushReplacement(
      //       MaterialPageRoute(
      //         builder: (context) => LoginPage(),
      //       ),
      //     );
      //   } else if (state is AuthBlocLoggedInState) {
      //     Navigator.of(context)
      //         .push(MaterialPageRoute(builder: (context) => HomeBlocScreen()));
      //   } else if (state is AuthBlocLoggedOutState) {
      //     Navigator.of(context).pushAndRemoveUntil(
      //         MaterialPageRoute(builder: (context) => SplashScreenPage()),
      //         (Route<dynamic> route) => false);
      //   }
      // }, builder: (context, state) {
      //   return FutureBuilder(
      //       future: getIt.allReady(),
      //       builder: (context, snp) {
      //         if (snp.hasData) {
      //           return SplashScreenPage();
      //         } else {
      //           return Container();
      //         }
      //       });
      // }),
    );
  }
}
