import 'package:injectable/injectable.dart';
import 'package:majootestcase/services/user_database.dart';
import 'package:sqflite/sqlite_api.dart';

@module
abstract class DatabaseModule {
  UserDatabase get database => UserDatabase.instance;
}
