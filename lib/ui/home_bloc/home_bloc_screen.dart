
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/injectable_config.dart';
import '../../bloc/home_bloc/home_bloc_cubit.dart';
import 'home_bloc_loaded_screen.dart';
import '../extra/loading.dart';
import '../extra/error_screen.dart';

class HomeBlocScreen extends StatelessWidget {
  static final String routeName = '/home-bloc';
  const HomeBlocScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (_) => getIt<HomeBlocCubit>()..fetchingData(),
        child: BlocBuilder<HomeBlocCubit, HomeBlocState>(
            builder: (context, state) {
          if (state is HomeBlocLoadedState) {
            return HomeBlocLoadedScreen(data: state.data);
          } else if (state is HomeBlocLoadingState) {
            return LoadingIndicator();
          } else if (state is HomeBlocInitialState) {
            return LoadingIndicator();
          } else if (state is HomeBlocErrorState) {
            return ErrorScreen(
              message: state.error,
              fontSize: 30,
              gap: 10,
              retry: () {
                context.read<HomeBlocCubit>().fetchingData(); 
              },
            );
          }

          return Center(
              child: Text(kDebugMode ? "state not implemented $state" : ""));
        }));
  }
}
