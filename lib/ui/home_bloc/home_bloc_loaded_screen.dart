import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:majootestcase/models/movie_response.dart';
import 'package:majootestcase/ui/detail/movie_detail_page.dart';

class HomeBlocLoadedScreen extends StatelessWidget {
  final List<Data>? data;

  const HomeBlocLoadedScreen({Key? key, this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Home Screen"),
      ),
      body: ListView.builder(
        itemCount: data!.length,
        itemBuilder: (context, index) {
          print(data![index].id);
          return InkWell(
              onTap: () {
                Get.toNamed(MovieDetailPage.routeName,
                    arguments: [data![index], data]);
              },
              child: movieItemWidget(data![index]));
        },
      ),
    );
  }

  Widget movieItemWidget(Data data) {
    return Card(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(25.0))),
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.all(25),
            child: Image.network(data.i!.imageUrl!),
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 8, horizontal: 20),
            child: Text(data.l!, textDirection: TextDirection.ltr),
          )
        ],
      ),
    );
  }
}
