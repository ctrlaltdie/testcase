import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/models/user.dart';
import 'package:get/get.dart';
import 'package:majootestcase/ui/home_bloc/home_bloc_screen.dart';
import 'package:majootestcase/ui/signup/signup_page.dart';

import '../../injectable_config.dart';

class LoginPage extends StatefulWidget {
  static const String routeName = '/login';

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<LoginPage> {
  final _email = TextEditingController();
  final _password = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  bool _isPassword = true;

  AutovalidateMode? _autovalidateMode;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider(
        create: (context) => getIt<AuthBlocCubit>(),
        child: BlocListener<AuthBlocCubit, AuthBlocState>(
          listener: (context, state) {
            print(state);
            if (state is AuthBlocLoggedInState) {
              Get.offAndToNamed(HomeBlocScreen.routeName);
              Get.showSnackbar(GetSnackBar(
                margin: EdgeInsets.all(20),
                borderRadius: 10,
                backgroundColor: Colors.green,
                message: "Login Berhasil",
                duration: Duration(seconds: 4),
              ));
            }
            if (state is AuthBlocErrorState) {
              Get.showSnackbar(GetSnackBar(
                margin: EdgeInsets.all(20),
                borderRadius: 10,
                backgroundColor: Colors.red,
                message: "Login gagal , periksa kembali inputan anda",
                duration: Duration(seconds: 4),
              ));
            }
          },
          child: BlocBuilder<AuthBlocCubit, AuthBlocState>(
            builder: (context, state) {
              return SingleChildScrollView(
                child: Padding(
                  padding:
                      EdgeInsets.only(top: 75, left: 25, bottom: 25, right: 25),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'Selamat Datang',
                        style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.bold,
                          // color: colorBlue,
                        ),
                      ),
                      Text(
                        'Silahkan login terlebih dahulu',
                        style: TextStyle(
                          fontSize: 15,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      SizedBox(
                        height: 9,
                      ),
                      Form(
                        key: _formKey,
                        autovalidateMode: _autovalidateMode,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            TextFormField(
                              controller: _email,
                              validator: (validator) {
                                if (!validator.toString().isEmail) {
                                  return "Email is not valid";
                                } else {
                                  return null;
                                }
                              },
                              decoration: InputDecoration(
                                  prefixIcon: const Icon(Icons.email),
                                  labelText: "Email",
                                  hintText: "Username atau email",
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(3))),
                            ),
                            const SizedBox(height: 30),
                            TextFormField(
                              controller: _password,
                              obscureText: _isPassword,
                              obscuringCharacter: "*",
                              validator: (validator) {
                                if (validator.toString().length < 6) {
                                  return "Password minimal 6 karakter";
                                } else {
                                  return null;
                                }
                              },
                              decoration: InputDecoration(
                                  prefixIcon:
                                      const Icon(Icons.security_rounded),
                                  suffixIcon: IconButton(
                                    onPressed: () {
                                      setState(() {
                                        _isPassword = !_isPassword;
                                      });
                                    },
                                    icon: Icon(_isPassword
                                        ? Icons.visibility_off
                                        : Icons.visibility),
                                  ),
                                  labelText: "Password",
                                  hintText: "Lebih dari 6 karakter",
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(3))),
                            ),
                            const SizedBox(height: 30),
                            SizedBox(
                              width: double.infinity,
                              child: BlocBuilder<AuthBlocCubit, AuthBlocState>(
                                builder: (context, state) {
                                  if (state is AuthBlocLoadingState) {
                                    return ElevatedButton.icon(
                                      icon: Transform.scale(
                                          scale: 0.8,
                                          child: CircularProgressIndicator()),
                                      label: Text("Loading . . ."),
                                      onPressed: null,
                                    );
                                  } else
                                    return ElevatedButton(
                                      child: Text("SIGNIN"),
                                      onPressed: () => handleLogin(context),
                                    );
                                },
                              ),
                              // child:
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 50),
                      _register(),
                    ],
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  Widget _register() {
    return Align(
      alignment: Alignment.center,
      child: InkWell(
        onTap: () {
          Get.toNamed(SignupPage.routeName);
        },
        child: RichText(
          text: TextSpan(
              text: 'Belum punya akun? ',
              style: TextStyle(color: Colors.black45),
              children: [
                TextSpan(
                  text: 'Daftar',
                ),
              ]),
        ),
      ),
    );
  }

  void handleLogin(BuildContext context) async {
    setState(() {
      _autovalidateMode = AutovalidateMode.onUserInteraction;
    });
    if (_formKey.currentState!.validate()) {
      //all data validated
      context.read<AuthBlocCubit>().loginUser(_email.text, _password.text);
    } else {
      Get.showSnackbar(GetSnackBar(
        margin: EdgeInsets.all(20),
        borderRadius: 10,
        backgroundColor: Colors.red,
        message:
            "Form tidak boleh kosong, mohon cek kembali data yang anda inputkan",
        duration: Duration(seconds: 4),
      ));
    }
  }
}
