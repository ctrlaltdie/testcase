import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/models/movie_response.dart';
import 'package:get/get.dart';

import '../../injectable_config.dart';

class MovieDetailPage extends StatefulWidget {
  static const String routeName = '/movie-detail';
  const MovieDetailPage({Key? key}) : super(key: key);

  @override
  _MovieDetailPageState createState() => _MovieDetailPageState();
}

class _MovieDetailPageState extends State<MovieDetailPage> {
  late Data data;
  late List<Data> listData;
  @override
  void initState() {
    var _arg = Get.arguments as List<dynamic>;
    data = _arg[0] as Data;
    listData = _arg[1] as List<Data>;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Detail"),
        ),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Image.network(data.i!.imageUrl!),
              Card(
                child: ListTile(
                  trailing: Text(data.year.toString()),
                  title: Text(
                    data.l!,
                  ),
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 10, vertical: 15),
                child: Text(
                  "Series",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17),
                ),
              ),
              Container(
                height: 155,
                child: ListView.builder(
                  itemCount: listData.length,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, index) {
                    var _single = listData[index];
                    return Card(
                      clipBehavior: Clip.hardEdge,
                      elevation: 2,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8)),
                      child: Container(
                        width: 130,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Expanded(
                              flex: 4,
                              child: Image.network(
                                _single.i!.imageUrl!,
                                fit: BoxFit.cover,
                                width: double.infinity,
                              ),
                            ),
                            SizedBox(height: 5),
                            Expanded(
                              flex: 2,
                              child: Padding(
                                padding:
                                    const EdgeInsets.only(left: 8, right: 8),
                                child: Text(
                                  _single.l!,
                                  maxLines: 2,
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    );
                  },
                ),
              )
            ],
          ),
        ));
  }
}
