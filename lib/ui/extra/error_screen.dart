import 'package:flutter/material.dart';

class ErrorScreen extends StatelessWidget {
  final String message;
  final Function()? retry;
  final Color? textColor;
  final double fontSize;
  final double gap;
  final Widget? retryButton;

  const ErrorScreen(
      {Key? key,
      this.gap = 10,
      this.retryButton,
      this.message = "",
      this.fontSize = 14,
      this.retry,
      this.textColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              Icons.signal_wifi_bad_outlined,
              size: 120,
            ),
            Text(
              message,
              style: TextStyle(
                  fontSize: fontSize, color: textColor ?? Colors.black),
            ),
            retry != null
                ? Column(
                    children: [
                      SizedBox(
                        height: gap,
                      ),
                      Column(
                        children: [
                          retryButton ??
                              IconButton(
                                onPressed: () {
                                  if (retry != null) retry!();
                                },
                                icon: Icon(
                                  Icons.refresh_sharp,
                                  size: 40,
                                ),
                              ),
                          Text("retry")
                        ],
                      ),
                    ],
                  )
                : SizedBox()
          ],
        ),
      ),
    );
  }
}
