import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/models/user.dart';
import 'package:get/get.dart';

import '../../injectable_config.dart';

class SignupPage extends StatefulWidget {
  static const String routeName = '/signup';
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<SignupPage> {
  final _email = TextEditingController();
  final _name = TextEditingController();
  final _password = TextEditingController();
  final _confirmPassword = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  bool _isPassword = true;

  AutovalidateMode? _autovalidateMode;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider(
        create: (context) => getIt<AuthBlocCubit>(),
        child: BlocListener<AuthBlocCubit, AuthBlocState>(
          listener: (context, state) {
            print(state);
            if (state is AuthBlocRegisterFailState) {
              Get.showSnackbar(GetSnackBar(
                margin: EdgeInsets.all(20),
                borderRadius: 10,
                backgroundColor: Colors.red,
                message: state.error,
                duration: Duration(seconds: 4),
              ));
            }
            if (state is AuthBlocRegisterState) {
              Get.back();
              Get.showSnackbar(GetSnackBar(
                margin: EdgeInsets.all(20),
                borderRadius: 10,
                backgroundColor: Colors.green,
                message: "Berhasil register user\nSilahkan Login",
                duration: Duration(seconds: 4),
              ));
            }
          },
          child: BlocBuilder<AuthBlocCubit, AuthBlocState>(
            builder: (context, state) {
              return SingleChildScrollView(
                child: Padding(
                  padding:
                      EdgeInsets.only(top: 75, left: 25, bottom: 25, right: 25),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'Selamat Datang',
                        style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.bold,
                          // color: colorBlue,
                        ),
                      ),
                      Text(
                        'Silahkan daftar untuk membuat akun',
                        style: TextStyle(
                          fontSize: 15,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      SizedBox(
                        height: 9,
                      ),
                      Form(
                        key: _formKey,
                        autovalidateMode: _autovalidateMode,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            CustomTextField(
                              controller: _email,
                              iconData: Icons.email,
                              hint: "valid Email",
                              label: "Email",
                              validator: (validator) {
                                if (!validator.toString().isEmail) {
                                  return "Email is not valid";
                                } else {
                                  return null;
                                }
                              },
                            ),
                            const SizedBox(height: 30),
                            CustomTextField(
                              controller: _name,
                              iconData: Icons.person,
                              hint: "Masukan nama",
                              label: "Nama",
                              validator: (validator) {
                                if (validator.toString().isEmpty) {
                                  return "Nama tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                            ),
                            const SizedBox(height: 30),
                            CustomPasswordField(
                              controller: _password,
                              iconData: Icons.person,
                              hint: "Masukkan password",
                              label: "Password",
                              validator: (validator) {
                                if (validator.toString().length < 5) {
                                  return "Panjang username minimal 5 karakter";
                                } else {
                                  return null;
                                }
                              },
                            ),
                            const SizedBox(height: 30),
                            CustomPasswordField(
                              controller: _confirmPassword,
                              iconData: Icons.person,
                              hint: "Masukkan ulang password",
                              label: "Konfirmasi Password",
                              validator: (validator) {
                                if (validator.toString().length < 5) {
                                  return "Panjang username minimal 5 karakter";
                                } else if (_confirmPassword.text !=
                                    _password.text) {
                                  return "Password tidak sama";
                                } else {
                                  return null;
                                }
                              },
                            ),
                            const SizedBox(height: 30),
                            SizedBox(
                              width: double.infinity,
                              child: BlocBuilder<AuthBlocCubit, AuthBlocState>(
                                builder: (context, state) {
                                  if (state is AuthBlocLoadingState) {
                                    return ElevatedButton.icon(
                                      icon: Transform.scale(
                                          scale: 0.8,
                                          child: CircularProgressIndicator()),
                                      label: Text("Loading . . ."),
                                      onPressed: null,
                                    );
                                  } else
                                    return ElevatedButton(
                                      child: Text("SIGN UP"),
                                      onPressed: () => handleRegister(context),
                                    );
                                },
                              ),
                              // child:
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 50),
                      _login(),
                    ],
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  Widget _login() {
    return Align(
      alignment: Alignment.center,
      child: InkWell(
        onTap: () {
          Get.back();
        },
        child: RichText(
          text: TextSpan(
              text: 'Sudah punya akun? ',
              style: TextStyle(color: Colors.black45),
              children: [
                TextSpan(
                  text: 'Masuk',
                ),
              ]),
        ),
      ),
    );
  }

  void handleRegister(BuildContext context) async {
    setState(() {
      _autovalidateMode = AutovalidateMode.onUserInteraction;
    });
    if (_formKey.currentState!.validate()) {
      //all data validated
      var newUser =
          User(email: _email.text, password: _password.text, name: _name.text);

      context.read<AuthBlocCubit>().registerUser(newUser);
    } else {
      Get.showSnackbar(GetSnackBar(
        margin: EdgeInsets.all(20),
        borderRadius: 10,
        backgroundColor: Colors.red,
        message:
            "Form tidak boleh kosong, mohon cek kembali data yang anda inputkan",
        duration: Duration(seconds: 4),
      ));
    }
  }
}

class CustomPasswordField extends StatefulWidget {
  CustomPasswordField(
      {Key? key,
      required this.controller,
      this.validator,
      required this.iconData,
      required this.hint,
      required this.label})
      : super(key: key);
  final TextEditingController controller;
  final FormFieldValidator<String>? validator;
  final String label;
  final String hint;
  final IconData iconData;

  @override
  State<CustomPasswordField> createState() => _CustomPasswordFieldState();
}

class _CustomPasswordFieldState extends State<CustomPasswordField> {
  bool _isPassword = true;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      obscuringCharacter: "*",
      decoration: InputDecoration(
          prefixIcon: Icon(widget.iconData),
          suffixIcon: IconButton(
            onPressed: () {
              setState(() {
                _isPassword = !_isPassword;
              });
            },
            icon: Icon(_isPassword ? Icons.visibility_off : Icons.visibility),
          ),
          labelText: widget.label,
          hintText: widget.hint,
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(3))),
      obscureText: _isPassword,
      controller: widget.controller,
      validator: widget.validator,
    );
  }
}

class CustomTextField extends StatelessWidget {
  CustomTextField(
      {Key? key,
      required this.controller,
      this.validator,
      required this.iconData,
      required this.hint,
      required this.label})
      : super(key: key);
  final TextEditingController controller;
  final FormFieldValidator<String>? validator;
  final String label;
  final String hint;
  final IconData iconData;
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      validator: validator,
      decoration: InputDecoration(
          prefixIcon: Icon(iconData),
          labelText: label,
          hintText: hint,
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(3))),
    );
  }
}
