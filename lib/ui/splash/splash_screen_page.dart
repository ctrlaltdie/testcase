import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:get/get.dart';
import 'package:majootestcase/ui/home_bloc/home_bloc_screen.dart';
import 'package:majootestcase/ui/login/login_page.dart';

class SplashScreenPage extends StatefulWidget {
  static const String routeName = '/splash-screen';
  const SplashScreenPage({Key? key}) : super(key: key);

  @override
  _SplashScreenPageState createState() => _SplashScreenPageState();
}

class _SplashScreenPageState extends State<SplashScreenPage> {
  @override
  void initState() {
    context.read<AuthBlocCubit>().fetchHistoryLogin();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: BlocConsumer<AuthBlocCubit, AuthBlocState>(
            listener: (context, state) {
      if (state is AuthBlocLoginState) {
        Get.offAllNamed(LoginPage.routeName);
      } else if (state is AuthBlocLoggedInState) {
        Get.offAllNamed(HomeBlocScreen.routeName);
      } else if (state is AuthBlocLoggedOutState) {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => SplashScreenPage()),
            (Route<dynamic> route) => false);

        Get.offNamedUntil(SplashScreenPage.routeName,
            ModalRoute.withName(SplashScreenPage.routeName));
      }
    }, builder: (context, state) {
      return Center(
        child: CircularProgressIndicator(),
      );
    }));
  }
}
