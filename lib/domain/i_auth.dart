import 'package:dartz/dartz.dart';
import 'package:majootestcase/models/user.dart';

abstract class IAuth {
  Future<Either<String, bool>> fetchHistoryLogin();
  Future<Either<String, User>> loginuser(String email, String password);
  Future<Either<String, bool>> logOutUser();
  Future<Either<String, User>> registerNUser(User user);
}
