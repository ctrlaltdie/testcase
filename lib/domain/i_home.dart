import 'package:dartz/dartz.dart';
import 'package:majootestcase/models/movie_response.dart';
import 'package:majootestcase/utils/error_helper.dart';

abstract class IHome {
  Future<Either<String, MovieResponse?>> getMovieList();
}
