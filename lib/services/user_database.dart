import 'package:dartz/dartz.dart';
import 'package:majootestcase/models/movie_response.dart';
import 'package:majootestcase/models/user.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class UserDatabase {
  UserDatabase._init();
  static final UserDatabase instance = UserDatabase._init();
  static Database? _database;

  Future<Database> get database async {
    if (_database != null) return _database!;

    _database = await _initDB('notes.db');
    return _database!;
  }

  Future<Database> _initDB(String filePath) async {
    final dbPath = await getDatabasesPath();
    final path = join(dbPath, filePath);
    return await openDatabase(path, version: 1, onCreate: _createDB);
  }

  Future _createDB(Database db, int version) async {
    final pkType = 'TEXT PRIMARY KEY';
    final textType = 'TEXT NOT NULL';
    db.execute('''
    CREATE TABLE $tableUsers (
      ${UserFields.email} $pkType,
      ${UserFields.name} $textType,
      ${UserFields.password} $textType
    )
    ''');
  }

  Future<Either<String, User>> create(User user) async {
    final db = await instance.database;
    try {
      final id = await db.insert(tableUsers, user.toJson());
      if (id == 0) {
        throw "Conflict Data";
      }
      return Right(user);
    } on DatabaseException catch (e) {
      if (e.isUniqueConstraintError()) {
        return Left("Duplikat User/Email");
      }
      return Left(e.toString());
    }
  }

  Future<User> getLogin(String email, String password) async {
    final db = await instance.database;
    var res = await db.rawQuery(''' SELECT * FROM $tableUsers 
        WHERE ${UserFields.email} = '$email' 
        and ${UserFields.password} = '$password' ''');

    try {
      return new User.fromJson(res.first);
    } catch (e) {
      throw Exception("ID $email not found");
    }
  }

  Future<User> readUser(int id) async {
    final db = await instance.database;
    final maps = await db.query(tableUsers,
        columns: UserFields.values,
        where: '${UserFields.email} =  ?',
        whereArgs: [id]);

    if (maps.isNotEmpty) {
      return User.fromJson(maps.first);
    } else {
      throw Exception("ID $id not found");
    }
  }

  Future close() async {
    final db = await instance.database;
    db.close();
  }
}
