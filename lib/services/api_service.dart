import 'dart:async';
import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:majootestcase/domain/i_home.dart';
import 'package:majootestcase/models/movie_response.dart';
import 'package:majootestcase/utils/error_helper.dart';

@Injectable(as: IHome)
class ApiServices extends IHome {
  ApiServices(this.dio);
  final Dio dio;

  @override
  Future<Either<String, MovieResponse>> getMovieList() async {
    try {
      Response<String> response = await dio.get("");
      MovieResponse movieResponse =
          MovieResponse.fromJson(jsonDecode(response.data!));
      return right(movieResponse);
      // return left("e");
    } on DioError catch (e) {
      var _error = ErrorHelper.extractApiError(e);
      return left(_error);
    } catch (e) {
      return left(e.toString());
    }
  }
}
