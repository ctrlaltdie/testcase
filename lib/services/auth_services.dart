import 'dart:async';
import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:majootestcase/domain/i_auth.dart';
import 'package:majootestcase/injectable_config.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/services/user_database.dart';
import 'package:shared_preferences/shared_preferences.dart';

@Injectable(as: IAuth)
class AuthServices extends IAuth {
  AuthServices(this.prefs, this.userDb);
  final SharedPreferences prefs;
  final UserDatabase userDb;

  @override
  Future<Either<String, bool>> fetchHistoryLogin() async {
    // SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var isLoggedIn = prefs.getBool("is_logged_in");
    if (isLoggedIn == null) {
      return Left("Something wrong");
    } else {
      return Right(isLoggedIn);
    }
  }

  @override
  Future<Either<String, User>> loginuser(String email, String password) async {
    try {
      var _result = await userDb.getLogin(email, password);
      //simpan data ke preferences
      await prefs.setBool("is_logged_in", true);
      String data = _result.toJson().toString();
      await prefs.setString("user_value", data);
      return Right(_result);
    } catch (e) {
      return Left(e.toString());
    }
  }

  @override
  Future<Either<String, bool>> logOutUser() async {
    try {
      await prefs.setBool("is_logged_in", false);
      var _result = await prefs.clear();
      return Right(_result);
    } catch (e) {
      return Left(e.toString());
    }
  }

  @override
  Future<Either<String, User>> registerNUser(User user) async {
    try {
      var _result = await userDb.create(user);

      return (_result);
    } catch (e) {
      return Left(e.toString());
    }
  }
}
