import 'package:get/route_manager.dart';
import 'package:majootestcase/ui/detail/movie_detail_page.dart';
import 'package:majootestcase/ui/home_bloc/home_bloc_screen.dart';
import 'package:majootestcase/ui/login/login_page.dart';
import 'package:majootestcase/ui/signup/signup_page.dart';
import 'package:majootestcase/ui/splash/splash_screen_page.dart';

class Routes {
  static List<GetPage> _pages = [
    GetPage(name: SplashScreenPage.routeName, page: () => SplashScreenPage()),
    GetPage(name: SignupPage.routeName, page: () => SignupPage()),
    GetPage(name: MovieDetailPage.routeName, page: () => MovieDetailPage()),
    GetPage(name: LoginPage.routeName, page: () => LoginPage()),
    GetPage(name: HomeBlocScreen.routeName, page: () => HomeBlocScreen()),
  ];

  static List<GetPage> get getPages => _pages;
}
