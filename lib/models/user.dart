import 'package:equatable/equatable.dart';

final String tableUsers = 'users';

class UserFields {
  static final List<String> values = [email, name, password];
  static final String email = 'email';
  static final String name = 'name';
  static final String password = 'password';
}

class User extends Equatable {
  String? email;
  String? name;
  String? password;

  User({this.email, this.name, this.password});

  User.fromJson(Map<String, dynamic> json)
      : email = json['email'],
        password = json['password'],
        name = json['name'];

  Map<String, dynamic> toJson() => {
        UserFields.email: email,
        UserFields.password: password,
        UserFields.name: name,
      };

  User copyWith({
    String? email,
    String? name,
    String? password,
  }) {
    return User(
      email: email ?? this.email,
      name: name ?? this.name,
      password: password ?? this.password,
    );
  }

  @override
  List<Object> get props => [email!, name!, password!];
}
