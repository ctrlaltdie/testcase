import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:injectable/injectable.dart';
import 'package:majootestcase/domain/i_home.dart';
import 'package:majootestcase/models/movie_response.dart';

part 'home_bloc_state.dart';

@injectable
class HomeBlocCubit extends Cubit<HomeBlocState> {
  HomeBlocCubit(this.iHome) : super(HomeBlocInitialState());
  final IHome iHome;
  void fetchingData() async {
    emit(HomeBlocInitialState());
    var _result = await iHome.getMovieList();
    _result.fold(
      (l) => emit(HomeBlocErrorState("Error Unknown")),
      (r) => emit(HomeBlocLoadedState(r!.data)),
    );
  }
}
