part of 'auth_bloc_cubit.dart';

abstract class AuthBlocState extends Equatable {
  const AuthBlocState();

  @override
  List<Object> get props => [];
}

class AuthBlocInitialState extends AuthBlocState {}

class AuthBlocLoadingState extends AuthBlocState {}

class AuthBlocLoggedInState extends AuthBlocState {}

class AuthBlocLoggedOutState extends AuthBlocState {}

class AuthBlocLoginState extends AuthBlocState {}

class AuthBlocRegisterState extends AuthBlocState {
  final User user;
  AuthBlocRegisterState(this.user);
  @override
  List<Object> get props => [user];
}

class AuthBlocRegisterFailState extends AuthBlocState {
  final String error;
  AuthBlocRegisterFailState(this.error);
  @override
  List<Object> get props => [error];
}

class AuthBlocLoadedState extends AuthBlocState {
  final data;

  AuthBlocLoadedState(this.data);

  @override
  List<Object> get props => [data];
}

class AuthBlocErrorState extends AuthBlocState {
  final error;

  AuthBlocErrorState(this.error);

  @override
  List<Object> get props => [error];
}
