import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:injectable/injectable.dart';
import 'package:majootestcase/domain/i_auth.dart';
import 'package:majootestcase/models/user.dart';

part 'auth_bloc_state.dart';

@injectable
class AuthBlocCubit extends Cubit<AuthBlocState> {
  AuthBlocCubit(this.iAuth) : super(AuthBlocInitialState());
  final IAuth iAuth;

  void fetchHistoryLogin() async {
    emit(AuthBlocLoadingState());
    // SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var result = await iAuth.fetchHistoryLogin();
    result.fold(
      (l) => emit(AuthBlocLoginState()),
      (r) {
        if (r) {
          emit(AuthBlocLoggedInState());
        } else {
          emit(AuthBlocLoginState());
        }
      },
    );
  }

  void registerUser(User user) async {
    // SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    emit(AuthBlocLoadingState());
    var _registerd = await iAuth.registerNUser(user);
    await Future.delayed(Duration(seconds: 2));
    _registerd.fold(
      (l) => emit(AuthBlocRegisterFailState(l)),
      (r) => emit(AuthBlocRegisterState(r)),
    );
  }

  void loginUser(String email, String password) async {
    // SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    emit(AuthBlocLoadingState());
    var _result = await iAuth.loginuser(email, password);
    _result.fold(
      (l) => emit(AuthBlocErrorState(l)),
      (r) => emit(AuthBlocLoggedInState()),
    );
  }

  void logOutUser() async {
    // SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    // emit(AuthBlocLoadingState());
    await iAuth.logOutUser();
    //Delay just for the simulation
    await Future.delayed(Duration(seconds: 3));
    emit(AuthBlocLoggedOutState());
  }
}
